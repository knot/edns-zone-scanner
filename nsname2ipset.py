#!/usr/bin/python3

import ipaddress
import logging
import multiprocessing
import pickle
from typing import Dict, Set, Tuple

import dns.name
import dns.rdatatype
import dns.resolver

from evalzone import AnIPAddress


def yield_ns_name(nsnames, mapping):
    """
    returns: stream of NS names
    """
    for nsname in nsnames:
        if nsname not in mapping:  # no ips at all
            yield nsname

def resolve(qname: dns.name.Name, qtype) -> Set[AnIPAddress]:
    logging.debug('resolving %s %s', qname, qtype)
    try:
        answer = dns.resolver.query(qname, qtype)
    except (dns.resolver.NoAnswer, dns.resolver.NXDOMAIN):  # no A/AAAA
        return set()
    except dns.exception.DNSException as ex:
        #logging.exception(ex)
        raise
    else:
        return set(ipaddress.ip_address(ip.address) for ip in answer)

def get_ips(nsname: dns.name.Name) -> Tuple[dns.name.Name, Set[AnIPAddress]]:
    ips = set()  # type: Set[AnIPAddress]
    try:
        ips = resolve(nsname, dns.rdatatype.A)
        ips = ips.union(resolve(nsname, dns.rdatatype.AAAA))
    except Exception:  # return empty address set
        pass
    return nsname, ips

def load_nsnames() -> Set[dns.name.Name]:
    logging.info('loading NS list')
    with open('nslist.pickle', 'rb') as nslist_file:
        nslist = pickle.load(nslist_file)
    logging.info('loaded %s NS names', len(nslist))
    return nslist

def load_nsname2ipset() -> Dict[dns.name.Name, Set[AnIPAddress]]:
    logging.info('loading previous mapping')
    try:
        with open('nsname2ipset.pickle', 'rb') as nsname2ipset_file:
            mapping = pickle.load(nsname2ipset_file)
    except FileNotFoundError:
        mapping = {}
    return mapping

def update_mapping(nsnames: Set[dns.name.Name],
         mapping: Dict[dns.name.Name, Set[AnIPAddress]]) -> None:
    # workaround for mypy 0.650 which thinks that call to
    # reset_default_resolver() does not guarantee existence
    # of a default_resolver instance
    dns.resolver.default_resolver = dns.resolver.Resolver()
    dns.resolver.default_resolver.lifetime = 5
    #dns.resolver.default_resolver.nameservers = ['193.29.206.206']

    with multiprocessing.Pool(processes=128) as p:
        i = 0
        logging.info('starting DNS query machinery')
        for nsname, ipset in p.imap_unordered(get_ips, yield_ns_name(nsnames, mapping), chunksize=10):
            i += 1
            if i % 100 == 0:
                logging.info('queried %d names; %s out of %s NS names resolved to an IP address (%0.2f %%)',
                             i, len(mapping), len(nsnames), len(mapping)/len(nsnames)*100)
            if ipset:
                mapping[nsname] = ipset

def save(mapping: Dict[dns.name.Name, Set[AnIPAddress]]) -> None:
    logging.info('writting %d results', len(mapping))
    pickle.dump(mapping, open('nsname2ipset.pickle', 'wb'))

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
    multiprocessing.set_start_method('forkserver')
    nsnames = load_nsnames()
    mapping = load_nsname2ipset()
    try:
        update_mapping(nsnames, mapping)
    except Exception:
        logging.exception('exception while resolving names to IP addresses')
        raise
    finally:  # attempt to salvage partial results
        logging.info('%s out of %s NS names resolved to an IP address (%0.2f %%)',
                     len(mapping), len(nsnames), len(mapping)/len(nsnames)*100)
        save(mapping)
    # machine readable output: # of NS names resolved to an IP address
    print(len(mapping))
    # machine readable output: total # of NS names
    print(len(nsnames))
