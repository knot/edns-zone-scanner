#!/usr/bin/python3

import argparse
import collections
import ipaddress
import logging
import pickle
import re
import sys
from typing import Counter, Dict, FrozenSet, List, Tuple

from evalzone import Result, AnIPAddress

# hichina.com. @2400:3200:2000:59::1 (ns2.hichina.com.): tcp=failed do=timeout signed=timeout ednstcp=failed
# hichina.com. @106.11.211.54 (ns2.hichina.com.): tcp=connection-refused do=ok signed=ok ednstcp=connection-refused
# nic.cz. @193.29.206.1 (d.ns.nic.cz.): tcp=ok do=ok signed=ok,yes ednstcp=ok
# test. @127.0.0.10 (test.): tcp=nosoa,noaa do=nosoa,noaa signed=nosoa,noaa ednstcp=timeout

def parse_nsip_line(line: str) -> Tuple[AnIPAddress, Dict[str, FrozenSet[str]]]:
    """parse one line from genreport log"""
    matches = re.match('^[^ ]*\\. @(?P<ip>[^ ]+) \\([^)]+\\): (?P<results>.*=.*)$', line)
    if not matches:
        raise ValueError('line "{}" does not have expected format, skipping'.format(line))

    tests_list = matches.group('results').split()
    try:
        tests_results = {test.split('=')[0]:
                            frozenset(test.split('=')[1].split(','))
                         for test in tests_list}
    except IndexError:
        raise ValueError('skipping nonsense test results "{}"'.format(line))
    tests_results.pop('signed', None)  # ignore DNSSEC things
    return ipaddress.ip_address(matches.group("ip")), tests_results

def eval_tcp_strict(tcp_results: Dict[str, List[str]]) -> Result:
    """
    Evaluate impact of TCP failures on clients with small EDNS buffer size.
    Failure will prevent clients with small buffer size from retrieving data.
    """
    return Result.dead

def eval_tcp_permissive(tcp_results: Dict[str, List[str]]) -> Result:
    """
    Evaluate impact of TCP failures on clients with big EDNS buffer size.
    Timeouts force retries elsewhere and add high latency
    but it usually gets through.
    """
    if 'ok' in tcp_results['do']:
        return Result.half_dead  # EDNS over UDP works but TCP has issues
    else:
        return Result.dead

tcp_hard_breakage_types = frozenset(['timeout', 'failed', 'reset', 'connection-refused', 'eof',
    'malformed', 'mismatch', 'rcode15', 'servfail', 'refused', 'nxdomain'])
def eval_tcp(tests_results: Dict[str, FrozenSet[str]], timeout_evaluator) -> Result:
    """
    Combine individual tests into overall result for a single IP address.
    """
    if all('ok' in results for results in tests_results.values()):
        return Result.ok
    # it is not 100% compliant but answers in a reasonable way
    # -> it is kind of "compatible" but does not get highest grade
    if (all(tcp_hard_breakage_types.intersection(results) == 0
            for results in tests_results.values())):
        return Result.compatible
    else:  # impact of not working TCP is different before and after the flag day
        return timeout_evaluator(tests_results)

def eval_edns_strict(edns0_results: Dict[str, List[str]]) -> Result:
    """
    Evaluate EDNS0 query timeout impact after 2019 DNS flag day.
    Timeouts as result of EDNS 0 non-compliance will not cause retry.
    """
    return Result.dead

def eval_edns_permissive(edns0_results: Dict[str, List[str]]) -> Result:
    """
    Evaluate EDNS0 query timeout impact before 2019 DNS flag day.
    Timeouts as result of EDNS 0 non-compliance will trigger retry without EDNS.
    """
    if 'ok' in edns0_results['dns']:
        return Result.half_dead  # plain DNS works but EDNS forces retries
    else:
        return Result.dead

def eval_edns(tests_results: Dict[str, List[str]], timeout_evaluator) -> Result:
    """
    Combine individual tests into overall result for a single IP address.
    """
    if all('ok' in results for results in tests_results.values()):
        return Result.ok
    # ignore EDNS1, it is not affected by 2019 DNS flag day
    edns0_results = {testname: values
                     for testname, values in tests_results.items()
                     if 'edns1' not in testname}
    # if EDNS0 queries do not time out it is kind of "compatible" with DNS 2019 flag day
    if all('timeout' not in results for results in edns0_results.values()):
        return Result.compatible
    else:  # impact of timeouts is different before and after the flag day
        return timeout_evaluator(edns0_results)

def collect_server_stats(eval_func, eval_mode_func, edns_infns: str) -> Dict[AnIPAddress, Counter[Result]]:
    """
    Combine results from all files with genreport output and summarize stats
    """
    server_stats = {}  # type: Dict[AnIPAddress, Counter[Result]]
    i = 1
    for infilename in edns_infns:
        logging.info('processed file no. {}, file name "{}"'.format(i, infilename))
        with open(infilename) as infile:
            for line in infile:
                line = line.strip()
                try:
                    ip, edns_results = parse_nsip_line(line)
                    combined_result = eval_func(edns_results, eval_mode_func)
                    server = server_stats.setdefault(ip, collections.Counter())
                    server[combined_result] += 1
                except ValueError as ex:
                    logging.warning('%s', ex)
                    #raise
        i += 1
    return server_stats

def save(nsstats, testset: str, criteria: str) -> None:
    """
    param criteria: name of criteria - strict / permissive
    """
    filename = 'stats_{}_{}.pickle'.format(testset, criteria)
    logging.info('saving {} results into {}'.format(testset, filename))
    pickle.dump(nsstats, open(filename, 'wb'))

def main(testset, infiles):
    """
    infiles - names of files with output from ISC genreport
    """
    if testset == 'edns2019':
        eval_func = eval_edns
        eval_func_strict = eval_edns_strict
        eval_func_permissive = eval_edns_permissive
    else:
        eval_func = eval_tcp
        eval_func_strict = eval_tcp_strict
        eval_func_permissive = eval_tcp_permissive

    logging.info('processing input in strict mode')
    nsstats_strict = collect_server_stats(eval_func, eval_func_strict, infiles)
    save(nsstats_strict, testset, 'strict')
    logging.info('processing input in permissive mode')
    nsstats_permissive = collect_server_stats(eval_tcp, eval_func_permissive, infiles)
    save(nsstats_permissive, testset, 'permissive')
    return nsstats_strict, nsstats_permissive

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(message)s')
    argparser = argparse.ArgumentParser(description='preprocess results from genreport')
    argparser.add_argument('scan_type', choices=['edns2019', 'tcp'], help='test set used by parser')
    argparser.add_argument('report_files', nargs='+', help='output files generated by genreport')
    args = argparser.parse_args()

    main(args.scan_type, args.report_files)
