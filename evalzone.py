#!/usr/bin/python3

"""
produce stats summary from pickled statistical data
"""
import argparse
from enum import IntEnum
import collections
import ipaddress
import logging
import pickle
from typing import Counter, Dict, List, Iterable, Iterator, NamedTuple, Set, Union

import dns.name


AnIPAddress = Union[ipaddress.IPv4Address, ipaddress.IPv6Address]


class Result(IntEnum):
    """
    evaluation result for single zone
    """
    ok = 1  # passed all tests
    compatible = 2  # does not support all features but will work
    half_dead = 3  # timeouts are likely but it will work eventually
    dead = 4  # not even one NS works

    def __str__(self):
        """omit class name"""
        return self.name

ZoneNSStats = NamedTuple('ZoneNSStats', [('origin', dns.name.Name),
                                         # stats: ip -> # replies
                                         ('stats', Dict[str, int]),  # ip -> # replies
                                         ('one_ns_works', bool)])

NSIPResult = NamedTuple('NSIPResult', [('ip', str),
                                       ('result', Result)])

DomainResult = NamedTuple('DomainResult', [('state', Result),
                                           ('reason', str)])

ComparisonRow = NamedTuple('ComparisonRow', [('permissive', int),
                                           ('strict', int)])

def zone_stream(zone2ns_fn: str, threshold: int) -> Iterator[ZoneNSStats]:
    """
    Compute whether at least one NS for each zone is alive and
    yield zone info.
    """
    with open(zone2ns_fn, 'rb') as zone2ns_bin:
        zone2ns = pickle.load(zone2ns_bin)
    for name, stats in zone2ns.items():
        one_ns_works = any(n >= threshold for n in stats.values())
        yield ZoneNSStats(name, stats, one_ns_works)


def eval_ip(ip_results: Counter[Result]) -> Result:
    """combined result for single IP, majority wins"""
    return ip_results.most_common(1)[0][0]  # most common Result

def eval_nsset(nsips: Iterable[str], nsstats: Dict[str, Counter[Result]]) -> DomainResult:
    """combined result for the whole nsset"""
    set_results = []
    for ip in nsips:
        #try:  # this is an error in genreport input generation
        set_results.append(eval_ip(nsstats[ip]))
        #except KeyError as ex:
        #    print('IP {} not found in genreport stats'.format(ip))

    if len(set_results) == 0:
        raise KeyError('no IPs')
    if all(result == set_results[0] for result in set_results):
        # all IP have the same result
        return DomainResult(set_results[0], 'behavior consistent for all servers')
    if max(set_results) == Result.dead:
        # at least one works, others are dead
        return DomainResult(Result.half_dead, 'some servers are dead')
    return DomainResult(max(set_results), 'worst result from NS set')  # typically Result.compatible mixed with ok

def eval_domain(domain: dns.name.Name,
                domain2nsset: Dict[dns.name.Name, Set[dns.name.Name]],
                nsname2ipset: Dict[dns.name.Name, Set[str]],
                domain2ipset: Dict[dns.name.Name, Set[str]],
                stats: Dict[str, Counter[Result]]) -> DomainResult:
    # no NS RR
    if not domain in domain2nsset:
        assert False, 'how this could happen?'
        return DomainResult(Result.dead, 'no NS in parent zone')

    # NS do not point to working authoritative server
    ipset_real = domain2ipset.get(domain, None)
    if not ipset_real:
        return DomainResult(Result.dead, 'no working NS is authoritative for this domain')

    result = DomainResult(Result.ok, '')

    # at least one NS name does not have IP address at all
    for nsname in domain2nsset[domain]:
        if nsname not in nsname2ipset:
            result = DomainResult(Result.half_dead,
                                  'NS {} does not have an IP address'.format(nsname))

    # evaluate properties
    result = eval_nsset(ipset_real, stats)
    if result.state >= result.state:
        result = result

    return result

def eval_mode(
    stats: Dict[str, Counter[Result]],
    domain2nsset: Dict[dns.name.Name, Set[dns.name.Name]],
    nsname2ipset: Dict[dns.name.Name, Set[str]],
    domain2ipset: Dict[dns.name.Name, Set[str]]) \
    -> Dict[Result, Dict[dns.name.Name, str]]:

    results = {case: {} for case in Result}  # type: Dict[Result, Dict[dns.name.Name, str]]
    #errors = 0
    for domain in domain2nsset.keys():
        #try:  # this happens if genreport input does not contain all NS IP addresses
        domain_res = eval_domain(domain, domain2nsset, nsname2ipset, domain2ipset, stats)
        #except KeyError:
        #    errors += 1
        #    continue
        results[domain_res.state][domain] = domain_res.reason
    return results

def load(testset: str):
    logging.info('loading statistics for strict mode')
    with open('stats_{}_strict.pickle'.format(testset), 'rb') as stats_bin:
        stats_strict = pickle.load(stats_bin)
    logging.info('loading statistics for permissive mode')
    with open('stats_{}_permissive.pickle'.format(testset), 'rb') as stats_bin:
        stats_permissive = pickle.load(stats_bin)
    logging.info('loading NS sets')
    with open('domain2nsset.pickle', 'rb') as domain2nsset_bin:
        domain2nsset = pickle.load(domain2nsset_bin)
    logging.info('loading NS IP addresses')
    with open('nsname2ipset.pickle', 'rb') as nsname2ipset_bin:
        nsname2ipset = pickle.load(nsname2ipset_bin)
    logging.info('loading domain-IP mapping')
    with open('domain2ipset.pickle', 'rb') as domain2ipset_bin:
        domain2ipset = pickle.load(domain2ipset_bin)
    return stats_strict, stats_permissive, domain2nsset, nsname2ipset, domain2ipset

class ResultTable():
    header = ['Mode', 'Before flag day', 'After flag day']
    names = collections.OrderedDict([
        (Result.ok, 'Ok'),
        (Result.compatible, 'Compatible'),
        (Result.half_dead, 'Partially dead'),
        (Result.dead, 'Dead')
        ])

    def __init__(self, total, results_permissive, results_strict):
        self.results = {}
        self.total = total
        for category in self.names:
            self.results[category] = \
                ComparisonRow(len(results_permissive[category]),
                              len(results_strict[category]))

    @property
    def csv(self) -> str:
        lines = [';'.join(self.header)]
        for mode, name in self.names.items():
            lines.append('{};{};{}'.format(name,
                self.results[mode].permissive, self.results[mode].strict))
        return '\n'.join(lines)

    @property
    def text(self) -> str:
        output =   '{:15s}| {:>20s}  | {:>20s}\n'.format(*self.header)
        output += '{:.15s}+-{:.20s}--+--{:.20s}\n'.format(*['-'*30]*3)
        for mode, modename in self.names.items():
            perm_pass = self.results[mode].permissive
            strict_pass = self.results[mode].strict
            output += '{:15s}| {:>11n} {:>7.2f} % | {:>11n} {:>7.2f} %\n'.format(
               modename,
               perm_pass, perm_pass/self.total*100,
               strict_pass, strict_pass/self.total*100)
        return output

def evaluate(stats_strict, stats_permissive, domain2nsset, nsname2ipset, domain2ipset):
    logging.info('starting strict mode evaluation')
    results_strict = eval_mode(stats_strict, domain2nsset, nsname2ipset, domain2ipset)
    logging.debug('%s', {case: len(results_strict[case]) for case in Result})

    logging.info('starting permissive mode evaluation')
    results_permissive = eval_mode(stats_permissive, domain2nsset, nsname2ipset, domain2ipset)
    logging.debug('%s', {case: len(results_permissive[case]) for case in Result})

    total = len(domain2nsset)
    summary = ResultTable(total, results_permissive, results_strict)

    return summary, results_strict, results_permissive

def save_pickle(results, testset: str, mode: str) -> None:
    logging.info('pickling results for {} mode'.format(mode))
    with open('results_{}_{}.pickle'.format(testset, mode), 'wb') as results_bin:
        pickle.dump(results, results_bin)

def save_summary(summary, testset: str) -> None:
    with open('summary_{}.txt'.format(testset), 'w') as summary_file:
        summary_file.write(summary.text)
    with open('summary_{}.csv'.format(testset), 'w') as summary_file:
        summary_file.write(summary.csv)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')

    argparser = argparse.ArgumentParser(description='evaluate state of domains in zone')
    argparser.add_argument('scan_type', choices=['edns2019', 'tcp'], help='set of tests to evaluate')
    args = argparser.parse_args()

    summary, results_strict, results_permissive = evaluate(*load(args.scan_type))
    save_pickle(results_strict, args.scan_type, 'strict')
    save_pickle(results_permissive, args.scan_type, 'permissive')
    save_summary(summary, args.scan_type)
    print(summary.text)
