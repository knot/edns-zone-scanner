Usage
=====
There are two main options how to use the scanner:

a. Use script "allinone.py" to automate whole scan.
   This is recommended method and easiest to use.

b. Run individual parts of the scan by hand which allows you to inspect
   results from each individual run, to have control over number
   of test cycles etc.
   This is normally not required and is intended for development and debugging.


Preparation
-----------
0. Beware! Processing huge zone file requires several gigabytes
   of operating memory and it might take tens of minutes
   to convert the data from text to binary. Use a beefy machine.
   E.g. net. zone requires a machine with 16 GB of operating memory.
   Do not even try com. zone, it will blow up your machine
   - it is not optimized enough.

1. All the tools work with data in current working directory.
   Make sure it is writeable and has enough free space (comparable
   to size of the original zone file).

2. Make sure all prerequisites are met.
   It is important to check network requirements listed
   in file doc/prerequisites.rst!

   Once network is ready it might be easiest to use Docker image from CZ.NIC::

   $ sudo docker run -ti --network=host -v /home/test:/data registry.labs.nic.cz/knot/edns-zone-scanner/prod

   (This example mounts directory /home/test from host to /data inside the container.
    Copy your zone file into /home/test before you proceed.)

3. Canonicalize the zone file and strip out unnecessary data
   to speed up further processing. Do not skip this step,
   MISSING CANONICALIZATION WILL CAUSE PROBLEMS down the road::

   $ ldns-read-zone -zc -E SOA -E NS -E A -E AAAA input_zone > zone.nodnssec


Running scan
------------
Usage::

   $ allinone.py tcp <canonicalized zone file> <zone origin>

Example::

   $ allinone.py tcp zone.nodnssec example.net.

Once the zone is loaded into memory the script will print informational
messages about progress. Make a coffee or let it run overnight ...


Reading results
---------------
First of all remember to read file doc/methodology_tcp.rst.

Statistical results are stored in files summary_tcp.csv and summary_tcp.txt.

Example summary_tcp.txt::

   Mode           |      Before flag day  |       After flag day
   ---------------+-----------------------+----------------------
   Ok             |         191   82.68 % |         191   82.68 %
   Compatible     |           0    0.00 % |           0    0.00 %
   Partially dead |          39   16.88 % |          38   16.45 %
   Dead           |           1    0.43 % |           2    0.87 %

This table indicates that 1 domain is already dead and that 1 other domain
will die after the TCP flag day. 191 domains is 100 % compliant,
and remaining 38 domains have problems unrelated to DNS flag day
on subset of their name servers.

To get list of domains which will die after the DNS flag day run::

   $ printresults.py tcp new
   strict dead 48.in-addr.arpa. ; behavior consistent for all servers

To get list of domains which are dead already (even before the flag day)
along with their NS names run::

   $ printresults.py tcp all permissive dead --ns
   permissive dead 55.in-addr.arpa. ns01.army.mil. ; no working NS is authoritative for this domain
   permissive dead 55.in-addr.arpa. ns02.army.mil. ; no working NS is authoritative for this domain
   permissive dead 55.in-addr.arpa. ns03.army.mil. ; no working NS is authoritative for this domain

That's it! Thank you for helping out with the DNS flag day!


Manual run
----------
Alternative to "allinone.py" script is to run individual the tools in sequence
to get statistical results for the whole zone.

Steps which require communication across network should be
run multiple times to smooth out network glitches like timeouts etc.

With all this in mind you can use the following script.
Please read comments below and report bugs to CZ.NIC Gitlab:
https://gitlab.labs.nic.cz/knot/edns-zone-scanner/issues

(Do not read this in Gitlab's web interface, it is ugly!)

# get zone data into file called "zone", e.g.
# dig -t AXFR nu. @zonedata.iis.se. nu. > zone
wget -O zone 'https://www.internic.net/domain/in-addr.arpa'

# canonicalize the zone
# and strip DNSSEC records to speed up processing
ldns-read-zone -zc -E SOA -E NS -E A -E AAAA input_zone > zone.nodnssec

# transform zonefile into Python objects
# NOTE: change "<example.origin.>" to zone origin, e.g. "cz."
zone2pickle.py zone.nodnssec <example.origin.>

# resolve NS names to IP addresses using local resolver
# (timeouts etc. might cause some queries to fail)
# repeat the process until you have sufficient number of NS names resolved
nsname2ipset.py
# (see stats at the very end of output)

# determine IP addresses of authoritative NSses for each domain
# this step sends "<domain> NS" query to each IP address to test
# if given IP address is authoritative for given domain
# (timeouts etc. might cause some queries to fail)
# repeat the process until you have sufficient number of IP addresses tested
domain2ipset.py
# (see stats at the very end of output)

# generate input for compliance test suite
genednscomp.py > ednscomp.input

# run TCP compliance test suite
# the script runs genreport binary in a loop
# it is recommended to collect at least 10 full runs to eliminate network noise
# (feel free to terminate the script with SIGTERM)
# result of each run is stored in file tcpcompresult-<timestamp>
# Hint: You can execute rungenreport.py in parallel, possibly on multiple machines
PATH=$PATH:<path to genreport tool> rungenreport.py tcp
# (monitor number of tcpcompresult- files and terminate as necessary;
#  the script will do 10 full scans to eliminate random network failures)

# merge all text results from TCP test suite into Python objects
report2pickle.py tcp tcpcompresult-*

# process stats for given zone
evalzone.py tcp
# output includes statistical results for whole zone file

# print list of domains which are going to break in 2020
# i.e. list of domains which are clasified as "high latency"
# in the permissive mode but are "dead" in strict mode
printresults.py tcp new

# alternatively print dead domains + list of their NSses
# some of the NSes might be broken for other reasons than TCP,
# e.g. some might not be authoritative for domain in question etc.
printresults.py tcp new --ns
