Methodology
===========
This section roughly describes algorithm used to categorize domains.
Please note that categorization depends on "mode", i.e. results differ
for situation before the DNS Flag Day 2019 and after it. See below.

Assumptions
-----------
Beware that the algorithm is optimized using following assumption:
EDNS support on a given IP address does not depend on domain name
used for test as long as the IP address is authoritative
for the domain.

E.g. if two zones example.com. and example.net. are hosted at
the same IP address 192.0.2.1, it is expected that the IP address
exhibits the same behavior if tests are done with example.com.
and example.net.

This assumption allows us to test each IP address just N times
intead of N*(number of domains hosted on that IP address).


Algorithm
---------
1. Each delegation (NS record) in the zone file is converted to mapping
   domain => set of NS names => set of IP addresses
   using glue data from the DNS zone + local resolver for names which do not
   have glue in the zone.

2. Each individual name server IP address is tested to check if the NS
   responds authoritatively for given domain. An IP is considered "dead"
   if it does not respond at all to plain DNS query "domain. NS"
   or if it is not authoritative for a given domain.

3. Each NS IP address which is authoritative for at least one domain
   is then tested for EDNS compliance using genreport tool by ISC.
   Each IP address is tested once during one pass, i.e. one NS which is
   authoritative for 300k domains in zone will be tested only once.

4. EDNS test can be repeated multiple times to eliminate effect
   of random network glitches to overall result.
   Multiple runs of individual tests for a single IP are combined using
   simple majority. This should eliminate random network failures.
   E.g. if genreport test 'edns' times out once and passes ('ok') 9 times
   only the 'ok' result is used.

5. For each IP address its individual EDNS test results from genreport
   are combined together to get overall state of that particular IP address:
   - if all tests are 'ok' -> overall result is 'ok'
   - further analysis ignores EDNS version 1 because it does not have impact
     on 2019 DNS flag day (i.e. only plain DNS and EDNS 0 is considered)
   - if no result is 'timeout' -> overall result is 'compatible'
     (it does not support all EDNS features but at least it does not break
      in face of EDNS 0 query)
   - further categorization depends on "mode", see below.

6. Evaluation in the "permissive" = state before the DNS Flag Day 2019:
   - IP addresses which pass only the basic test 'dns' but fail other tests
     with 'timeout' will eventually work and are categorized as 'half_dead'.
   - IP addresses which do not pass even 'dns' test are categorized as 'dead'.

7. Evaluation in the "strict" mode = state after the DNS Flag Day 2019:
   - IP addresses which fail any DNS or EDNS 0 test with 'timeout'
     are categorized as 'dead'. In strict mode there is no 'half_dead'
     caused by EDNS non-compliance.

8. Results for individual IP addresses are combined to overall result for each
   domain delegated in the zone file:
   - domains without any working authoritative NS are 'dead'
   - remaining domains with all NS IP addresses 'dead' are 'dead'
     (IP evaluation depends on mode, see above)
   - remaining domains with at least one un-resolvable NS IP address
     + remaining domains with at least one NS IP address 'dead'
     are 'half_dead' (resolvers must retry queries)
   - remaining domains have their results set to the worst result
     from their respective NS set
     (e.g. 2 IP addresses 'ok' + 2 'compatible' => 'compatible')


Limitations
-----------
1. This toolchain tests EDNS compliance only on DNS delegations in given zone
and does not evaluate any other data.

For example, the DNS domain `example.com.` might contain this CNAME record:
`www.example.com. CNAME broken.cdn.test.`

If the tested zone file contains delegation `example.com. NS`,
the result will show only state of `example.com.`'s DNS servers
but will not reflect state of the target CDN which might be source
of EDNS compliance problems. As a result, the domain `example.com.`
could be categorized as `ok` but application running on `www.example.com.`
might be unavailable because of depedency on a broken CDN.


2. Anycast routing limits what can be tested from a single vantage point.
It is technically possible for authoritatives to use different implementations
in different anycast domains.

3. Of course, when evaluating impact it needs to be taken into accoutn that
not all domains are equally important for users.
