Prerequisites
=============

Environment requirements
------------------------
Before testing please make sure that:

- IPv4 and IPv6 connectivity actually works
- firewall and/or middlebox on network is not filtering any DNS packets
- IP fragments can be delivered back to the testing machine

The scanner tool does not check any of these and failure to provide
"clean" network path will significantly skew results.


Software dependencies
---------------------
Easiest way how to get the tool up and running is to use Docker image
from CZ.NIC Docker registry::

   $ sudo docker run --network=host -v /home/test:/data registry.labs.nic.cz/knot/edns-zone-scanner/prod

The image has all the tools installed in /usr/local/bin and is ready to run.


If you want to install everything yourself you will need:

1. The EDNS compliance test for single domain is actually done by
   ISC's tool genreport which is available from this URL:
   https://gitlab.isc.org/isc-projects/DNS-Compliance-Testing

2. python-dns library for Python 3
   Beware: Latest version of python-dns library will not work if you do not
   canonicalize zone files with some non-ASCII values.
   This breaks processing on certain TLDs so always canonicalize the zone file.

3. For big zones it is advisable to compile python-dns library using Cython
   as it provides ~ 30 % speedup. (The Docker image already has it.)::

   $ pip3 install --install-option="--cython-compile" git+https://github.com/rthalley/dnspython.git

4. ldns command line tool ldns-read-zone for zone canonicalization
   and to strip out unnecessary data.
