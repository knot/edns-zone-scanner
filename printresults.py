#!/usr/bin/python3
"""
print list of new "dead" domains + their NSes
i.e. list of domains which will break after 2019 EDNS Flag Day
"""

import argparse
import logging
import pickle
import sys
from typing import Dict, Optional, Set

import dns.name

from evalzone import Result

def print_domain(mode: str, result: Result, domain: dns.name.Name,
    nsset: Optional[Set[dns.name.Name]], reason) \
    -> None:
    if not nsset:
        print(mode, result, domain, ';', reason)
    else:
        for nsname in nsset:
            print(mode, result, domain, nsname, ';', reason)

def new_domains(permissive, strict,
    args, domain2ns: Dict[dns.name.Name, Set[dns.name.Name]]) -> None:
    logging.info('computing domains with NS which are going to stop working')
    edns_broken_domains = set(strict[Result.dead].keys()) \
            - set(permissive[Result.dead].keys())
    for domain in edns_broken_domains:
        nsset = args.ns and domain2ns[domain]
        print_domain('strict', Result.dead, domain, nsset, strict[Result.dead][domain])

def print_all(permissive, strict, args, domain2ns) -> None:
    if not args.mode:
        modes = {'permissive': permissive,
                 'strict': strict}
    else:
        modes = {args.mode: locals()[args.mode]}

    if not args.result:
        args.result = Result
    else:
        # workaround for non-functional argparse choices=Enum
        args.result = (getattr(Result, args.result), )

    for modename, modedata in modes.items():
        for result in args.result:
            for domain, reason in modedata[result].items():
                nsset = args.ns and domain2ns[domain]
                print_domain(modename, result, domain, nsset, reason)

def main():
    """
    compute stats and print them to stdout
    """
    parser = argparse.ArgumentParser(description='''
print test result for each domain in format:
<mode> <result> <domain name> [ns name] ; commentary
''')
    parser.add_argument('scan_type', choices=['edns2019', 'tcp'], help='set of tests')
    subparsers = parser.add_subparsers(dest='cmd')

    allcmd = subparsers.add_parser('all', help='list results for all domains (see list --help)')
    allcmd.add_argument('mode', nargs='?', choices=['permissive', 'strict'],
                         help='limit listing to specified mode')
    # workaround for non-functional argparse choices=Enum
    allcmd.add_argument('result', nargs='?', choices=Result.__members__,
                         help='limit listing to specified result category')
    allcmd.add_argument('--ns', action='store_true', default=False,
                         help='print NS for each domain')
    allcmd.set_defaults(func=print_all)

    newcmd = subparsers.add_parser('new',
        help='list only domains which will die after 2019 DNS flag day')
    newcmd.add_argument('--ns', action='store_true', default=False,
                        help='print NS for each domain')
    newcmd.set_defaults(func=new_domains)
    args = parser.parse_args()
    # parser.add_subparsers() call is missing required= option in Python 3.6
    if not args.cmd:
        sys.exit('subcommand required, see --help for usage')


    # this can be optimized but it is probably not worth the effort
    logging.info('loading permissive mode results')
    with open('results_{}_permissive.pickle'.format(args.scan_type), 'rb') as pickle_bin:
        permissive = pickle.load(pickle_bin)
    logging.info('loading strict mode results')
    with open('results_{}_strict.pickle'.format(args.scan_type), 'rb') as pickle_bin:
        strict = pickle.load(pickle_bin)

    if args.ns:
        logging.info('loading NS sets')
        with open('domain2nsset.pickle', 'rb') as pickle_bin:
            domain2ns = pickle.load(pickle_bin)
    else:
        domain2ns = None

    args.func(permissive, strict, args, domain2ns)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(message)s')
    main()
