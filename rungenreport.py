#!/usr/bin/python3

import argparse
import datetime
import logging
import subprocess
import sys

def repeat_genreport(cycles: int, prefix: str, args: list):
    logging.info('Hint: use tail -f to monitor progress of individual genreport runs')
    with open('ednscomp.input', 'rb') as ednscomp_input:
        for cycle in range(1, cycles + 1):
            with open('{}compresult-{}'.format(
                    prefix,
                    datetime.datetime.utcnow().isoformat()),
                'wb') as ednscomp_output:
                logging.info('genreport round {} / {}, output file {}'.format(
                     cycle, cycles, ednscomp_output.name))
                subprocess.run(check=True, stdin=ednscomp_input, stdout=ednscomp_output,
                       args=['genreport'] + args)
            ednscomp_input.seek(0)

def check_env():
    # check if genreport is in PATH and can be executed
    try:
        subprocess.run(check=True, input='', args=['genreport'])
    except:
        logging.exception('unable to execute genreport tool, make sure it is in PATH')
        raise SystemExit('fatal error: genreport is required')

def run(cycles: int, scan_type: str):
    if scan_type == 'edns2019':
        genreport_args = ['-s', '-m', '500']
    elif scan_type == 'tcp':
        genreport_args = ['-i', 'do', '-i', 'ednstcp']
    else:
        raise NotImplementedError()

    repeat_genreport(cycles, scan_type, genreport_args)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
    check_env()

    argparser = argparse.ArgumentParser(description='test delegations in given zone file')
    argparser.add_argument('scan_type', choices=['edns2019', 'tcp'],
                           help='set of tests to run')
    argparser.add_argument('cycles', nargs='?', type=int, default=10,
                           help='criteria to use for scanner')
    args = argparser.parse_args()
    run(args.cycles, args.scan_type)
