#!/usr/bin/bash
set -o errexit -o xtrace

test -f zone || wget -O zone https://www.internic.net/domain/in-addr.arpa
ldns-read-zone -zc -E SOA -E NS -E A -E AAAA zone > zone.normalized
zone2pickle.py zone.normalized in-addr.arpa
nsname2ipset.py
domain2ipset.py
genednscomp.py > ednscomp.input
rungenreport.py tcp 2
report2pickle.py tcp tcpcompresult-*
evalzone.py tcp
printresults.py tcp new
printresults.py tcp new --ns
printresults.py tcp all permissive dead --ns
