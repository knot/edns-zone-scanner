#!/usr/bin/bash
set -o errexit -o xtrace

test -f zone || wget -O zone https://www.internic.net/domain/in-addr.arpa
ldns-read-zone -zc -E SOA -E NS -E A -E AAAA zone > zone.normalized
allinone.py tcp zone.normalized in-addr.arpa
printresults.py tcp new
printresults.py tcp new --ns
printresults.py tcp all permissive dead --ns
