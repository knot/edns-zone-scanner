#!/usr/bin/bash
set -o errexit -o xtrace

test -f zone || wget -O zone https://www.internic.net/domain/in-addr.arpa
ldns-read-zone -zc -E SOA -E NS -E A -E AAAA zone > zone.normalized
zone2pickle.py zone.normalized in-addr.arpa
nsname2ipset.py
domain2ipset.py
genednscomp.py > ednscomp.input
rungenreport.py edns2019 2
report2pickle.py edns2019 edns2019compresult-*
evalzone.py edns2019
printresults.py edns2019 new
printresults.py edns2019 new --ns
printresults.py edns2019 all permissive dead --ns
