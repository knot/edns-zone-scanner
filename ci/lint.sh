#!/bin/bash
set -o errexit -o xtrace
python3 -m pylint -E *.py
python3 -m mypy --ignore-missing-imports *.py
