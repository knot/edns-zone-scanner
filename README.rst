DNS compliance scanner for DNS zones
====================================

This repo contains set of scripts to scan all delegated domains from single
zone file for EDNS and DNS-over-TCP compliance problems and to evaluate
practical impact of EDNS Flag Days 2019 and 2020
(see https://dnsflagday.net/) on particular zone.

Testing methodology is described in file doc/methodology_edns2019.rst
and doc/methodology_tcp.rst.

Before you start please follow instructions in file doc/prerequisites.rst,
it contains important information.

Usage instructions can be found in file doc/usage.rst.

Please report bugs to CZ.NIC Gitlab:
https://gitlab.labs.nic.cz/knot/edns-zone-scanner/issues

Thank you for helping out with the DNS flag day!
