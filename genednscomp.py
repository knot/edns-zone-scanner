#!/usr/bin/python3
"""
Generate input for ednscomp.
"""

import logging
from typing import Dict, Set

import dns.name

import dataapi
from evalzone import AnIPAddress

def gen_ip_to_nsname(nsname2ipset: Dict[dns.name.Name, Set[AnIPAddress]]) -> Dict[AnIPAddress, dns.name.Name]:
    """
    Generate reverse mappping IP -> NS name.
    """
    ip2nsname = {}
    for nsname, ipset in nsname2ipset.items():
        ip2nsname.update({ip: nsname for ip in ipset})
    return ip2nsname

def generate(nsname2ipset, domain2ipset):
    """Output format is: zone nsname NSipAddress"""
    ip_done = set()
    ip2nsname = gen_ip_to_nsname(nsname2ipset)

    for domain, ipset in domain2ipset.items():
        for ipaddr in ipset:
            if ipaddr in ip_done:
                continue
            # It should never happen that ipaddr is not present in nsname2ipset mapping.
            # If it happens, it is most likely caused by mixing data files from
            # two versions of DNS zone. Start from scratch!
            nsname = ip2nsname[ipaddr]
            yield '{} {} {}\n'.format(domain, nsname, ipaddr)
            ip_done.add(ipaddr)

def main():
    """stand-alone operation if allinone module is not used"""
    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')

    nsname2ipset = dataapi.load_nsname2ipset()
    domain2ipset = dataapi.load_domain2ipset()

    for genreport_line in generate(nsname2ipset, domain2ipset):
        print(genreport_line, end='')

if __name__ == "__main__":
    main()
