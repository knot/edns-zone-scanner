#!/usr/bin/python3
import argparse
import glob
import logging
import multiprocessing
import sys

import dns.name

import domain2ipset
import evalzone
import genednscomp
import nsname2ipset
import report2pickle
import rungenreport
import zone2pickle

def main():
    multiprocessing.set_start_method('forkserver')
    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')

    argparser = argparse.ArgumentParser(description='test delegations in given zone file')
    argparser.add_argument('scan_type', choices=['edns2019', 'tcp'], help='criteria to use for scanner')
    argparser.add_argument('zone_file', type=open, help='zone file in RFC 1035 format')
    argparser.add_argument('zone_origin', type=dns.name.from_text, help='zone name, SOA RR must be present')
    args = argparser.parse_args()

    domain_nsset, nsnames, nsname_ipsets = zone2pickle.convert(args.zone_file, args.zone_origin)
    zone2pickle.save(domain_nsset, nsnames, nsname_ipsets)

    logging.info('resolving NS names to IP addresses')
    # repeat until
    # a) all names are resolved
    # b) last run did not bring new IP addresses
    while True:
        prev_ip_cnt = len(nsname_ipsets)
        nsname2ipset.update_mapping(nsnames, nsname_ipsets)
        total = len(nsnames)
        remaining = total - len(nsname_ipsets)
        new = len(nsname_ipsets) - prev_ip_cnt
        if remaining == 0:
            logging.info('all NS names resolved to an IP address')
            break
        elif new < total / 100 / 100:  # ignore last < 0.01 %
            logging.info('unable to resolve last %d NS names to an IP address, '
                         'leaving %0.2f %% NS names unresolved',
                         remaining,
                         remaining/total * 100)
            break
        else:
            logging.info('resolved new %d NS names (%0.2f %%) to an IP address, '
                         'retryring resolution for remaining %d NS names (%0.2f %%)',
                         new,
                         new / total * 100,
                         remaining,
                         remaining/total * 100)
    nsname2ipset.save(nsname_ipsets)

    # domain2ipset.py
    logging.info('looking for a working NS IP addresses for each domain')
    netstats = domain2ipset.NetStats()
    domain_ipset = {}
    while True:
        prev_ip_cnt = len(domain_ipset)
        domain2ipset.update_mapping(domain_nsset, nsname_ipsets, netstats, domain_ipset)
        total = len(domain_nsset)
        remaining = total - len(domain_ipset)
        new = len(domain_ipset) - prev_ip_cnt
        if remaining == 0:
            logging.info('all domains have at least one NS IP address which responds')
            break
        elif new < total / 100 / 100:  # ignore last < 0.01 %
            logging.info('unable to find NS IP addresses for last %d domains, '
                         'leaving %0.2f %% domains without working NS IP address',
                         remaining,
                         remaining/total * 100)
            break
        else:
            logging.info('found working NS IP address for %d domains (%0.2f %%), '
                         'retryring resolution for remaining %d domains (%0.2f %%)',
                         new,
                         new / total * 100,
                         remaining,
                         remaining/total * 100)
    domain2ipset.save(domain_nsset, netstats, domain_ipset)

    logging.info('generating input data for genreport tool')
    with open('ednscomp.input', 'w') as ednscomp_input:
        ednscomp_input.writelines(genednscomp.generate(nsname_ipsets, domain_ipset))

    logging.info('executing {} tests'.format(args.scan_type))
    rungenreport.run(10, args.scan_type)
    globname = '{}compresult-*'.format(args.scan_type)
    ednscompresults = glob.glob(globname)
    if not ednscompresults:
        logging.critical('error: no {} files from previous step found, exiting'.format(globname))
        sys.exit(2)

    nsstats_strict, nsstats_permissive = report2pickle.main(args.scan_type, ednscompresults)

    summary, results_strict, results_permissive = evalzone.evaluate(nsstats_strict, nsstats_permissive, domain_nsset, nsname_ipsets, domain_ipset)
    evalzone.save_pickle(results_strict, args.scan_type, 'strict')
    evalzone.save_pickle(results_permissive, args.scan_type, 'permissive')
    evalzone.save_summary(summary, args.scan_type)
    print(summary.text)


if __name__ == "__main__":
    rungenreport.check_env()
    main()
