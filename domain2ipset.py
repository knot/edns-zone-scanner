#!/usr/bin/python3
"""
Determine set working IP addresses for all domains in zone.

It is necessary to find a domain name which works with plain DNS so this
script sends queries to IP addresses from glue.
"""

import collections
import enum
import itertools
import logging
import multiprocessing
import pickle
import random
from typing import Counter, Deque, Dict, FrozenSet, Iterable, Set, Tuple
import weakref

import dns.message
import dns.query

from evalzone import AnIPAddress

class IP_state(enum.Enum):
    timeout = 0
    notauth = 1  # refused etc.
    ok = 2

class NetStats():
    def __init__(self):
        self.netstats = {}  # type: Dict[AnIPAddress, Counter[str]]

    def __len__(self):
        return len(self.netstats)

    def is_ip_dead(self, ip: AnIPAddress) -> bool:
        if not ip in self.netstats:
            self.netstats[ip] = collections.Counter()
        return self.netstats[ip]['timeouts_in_row'] >= 10

    def record_ip(self, ip: AnIPAddress, state: IP_state) -> None:
        if state == IP_state.timeout:
            self.netstats[ip]['timeouts'] += 1
            self.netstats[ip]['timeouts_in_row'] += 1
            if self.netstats[ip]['timeouts_in_row'] >= 10:
                logging.warning('ignoring IP %s because of excessive timeouts', ip)
        else:  # a reply, yay!
            self.netstats[ip]['replies'] += 1
            self.netstats[ip]['timeouts_in_row'] = 0

    def skip_dead_ips(self, candidates: Iterable[Tuple[int, dns.name.Name, AnIPAddress]]) \
            -> Iterable[Tuple[int, dns.name.Name, AnIPAddress]]:
        '''skip over candidates which target dead IP addresses'''
        for args in candidates:
            _, _, ip = args
            if not self.is_ip_dead(ip):
                yield args

def count_candidates(iterable):
    """
    not for general use, depletes generator
    """
    i = 0
    for item in iterable:
        i += 1
    return i

def check_availability(args: Tuple[int, dns.name.Name, AnIPAddress]) -> Tuple[int, dns.name.Name, AnIPAddress, IP_state]:
    attempt, domain, ip = args
    """
    check if server replies to domain. NS query
    """
    query = dns.message.make_query(domain, dns.rdatatype.NS)
    state = IP_state.notauth
    try:
        answer = dns.query.udp(query, str(ip), timeout=2)
        if answer.rcode() == dns.rcode.NOERROR:
            state = IP_state.ok
    except dns.exception.Timeout:
        state = IP_state.timeout
    except dns.exception.DNSException:
        pass  # TODO: verify if this is correct
    finally:
        logging.debug('test %s @%s NS: %s', domain, ip, state)
        return (attempt, domain, ip, state)

def gen_candidates(domain2nsset: Dict[dns.name.Name, Set[dns.name.Name]],
                   nsname2ipset: Dict[dns.name.Name, Set[AnIPAddress]],
                   netstats: NetStats,
                   retry_queue: Deque[Tuple[int, dns.name.Name, AnIPAddress]],
                   domain2ipset: Dict[dns.name.Name, Set[AnIPAddress]]
                  ) -> Iterable[Tuple[int, dns.name.Name, AnIPAddress]]:
    for domain, nsset in domain2nsset.items():
        if len(retry_queue) > 100:  # prevent deque from unlimited growth
            logging.info('retry queue full, processing queued retries now')
            yield from retry_candidates(retry_queue, netstats)
            logging.info('retry queue empty, resuming normal operation')
        for nsname in nsset:
            if not nsname in nsname2ipset:
                continue  # does not have resolvable IP address
            for ip in nsname2ipset[nsname]:
                if netstats.is_ip_dead(ip):
                    continue
                if domain in domain2ipset and ip in domain2ipset[domain]:
                    continue  # its already done
                # attempt, domain, ip
                yield (1, domain, ip)
    # final retry_candidates() cannot be here because
    # gen_candidates generator might pre-generate results and terminate
    # before retries are even generated

def retry_candidates(retry_queue: Deque[Tuple[int, dns.name.Name, AnIPAddress]],
                     netstats: NetStats
                    ) -> Iterable[Tuple[int, dns.name.Name, AnIPAddress]]:
    while retry_queue:
        attempt, domain, ip = retry_queue.popleft()
        if netstats.is_ip_dead(ip):
            continue  # dead IP was detected after insertion into retry queue
        if attempt >= 3:
            continue  # limit attempts
        logging.debug('retrying %s @%s (attempt no. %s)', domain, ip, attempt + 1)
        yield (attempt + 1, domain, ip)

def process_reply(attempt, domain, ip, state, netstats, retry_queue,
                    domain2ipset: Dict[dns.name.Name, FrozenSet[AnIPAddress]],
                    ipsetcache: Dict[int, FrozenSet[AnIPAddress]]):
        netstats.record_ip(ip, state)
        if state == IP_state.notauth:
            return  # not authoritative, look for another domain on this NS
        elif state == IP_state.timeout:
            retry_queue.append((attempt, domain, ip))
        elif state == IP_state.ok:
            # add IP address to set, re-use existing IP address sets to prevent memory bloat
            newset = frozenset((ip, )).union(domain2ipset.get(domain, ()))  # type: FrozenSet[AnIPAddress]
            domain2ipset[domain] = ipsetcache.setdefault(hash(newset), newset)
        #if len(ip_done) % 100 == 0:
        #    logging.info('generated output for %s IP addresses', len(ip_done))

def load():
    logging.info('loading NS sets')
    with open('domain2nsset.pickle', 'rb') as domain2nsset_pickle:
        domain2nsset = pickle.load(domain2nsset_pickle)

    logging.info('loading glue mapping')
    with open('nsname2ipset.pickle', 'rb') as nsname2ipset_pickle:
        nsname2ipset = pickle.load(nsname2ipset_pickle)
    try:
        with open('netstats.pickle', 'rb') as netstats_pickle:
            logging.info('loading network statistics from previous attempts')
            netstats = pickle.load(netstats_pickle)
    except FileNotFoundError:
        netstats = NetStats()

    try:
        with open('domain2ipset.pickle', 'rb') as domain2ipset_pickle:
            logging.info('loading domain-IP mapping from previous run')
            domain2ipset = pickle.load(domain2ipset_pickle)
    except FileNotFoundError:
        domain2ipset = {}  # type: Dict[dns.name.Name, FrozenSet[AnIPAddress]]

    return domain2nsset, nsname2ipset, netstats, domain2ipset

def save(domain2nsset, netstats, domain2ipset):
        logging.info('writting domain2ipset for %d domains', len(domain2ipset))
        pickle.dump(domain2ipset, open('domain2ipset.pickle', 'wb'))
        logging.info('writting network statistics for %d IPs', len(netstats))
        pickle.dump(netstats, open('netstats.pickle', 'wb'))
        logging.debug('dead domains: %s', domain2nsset.keys() - domain2ipset.keys())
        logging.info('%s out of %s domains has at least one working NS (%0.2f %%)',
                     len(domain2ipset), len(domain2nsset), len(domain2ipset)/len(domain2nsset)*100)

def randomize_iter(iterable, window_len: int):
    """Randomize order of iteration over an iterable using fixed window."""
    assert window_len > 0
    # buffer up to "window_len" items
    window = [True]
    while window:
        window = list(itertools.islice(iterable, window_len))
        random.shuffle(window)
        yield from window

def update_mapping(domain2nsset, nsname2ipset, netstats, domain2ipset):
    retry_queue = collections.deque()  # type: Deque[Tuple[int, dns.name.Name, str]]
    #logging.info('computing number of candidates to query')
    #candidates = gen_candidates(domain2nsset, nsname2ipset, netstats, retry_queue, domain2ipset)
    #logging.info('queue contains %s queries to be checked', count_candidates(candidates))


    # It is useful to avoid querying the same IP address 100 times in a row
    # because we could send all 100 queries in parallel and  blocking all
    # threads on waiting for (potential) timeout.
    #
    # With a sufficiently large window we should detect a dead IP address
    # before overshooting NetStats.timeouts_in_row limit and limit time wasted on timeouts.
    #
    # It could also help as a workaround to agressive response rate limiting.

    ipsetcache = weakref.WeakValueDictionary()  # type: Dict[int, FrozenSet[AnIpAddress]]
    candidates = netstats.skip_dead_ips(
                    randomize_iter(
                        gen_candidates(domain2nsset, nsname2ipset, netstats, retry_queue, domain2ipset),
                    100000))
    with multiprocessing.Pool(processes = 256) as pool:
        for attempt, domain, ip, state in pool.imap_unordered(check_availability, candidates):
            process_reply(attempt, domain, ip, state, netstats, retry_queue, domain2ipset, ipsetcache)
            if len(domain2ipset) % 1000 == 0 and len(domain2ipset.get(domain, [])) == 1:
                logging.info('%s domains out of %s have at least one working NS (%0.2f %%)',
                             len(domain2ipset), len(domain2nsset), len(domain2ipset)/len(domain2nsset)*100)
        logging.info('first pass done, processing queued retries')
        while retry_queue:  # retry_queue might be filled again in process_reply
            for attempt, domain, ip, state in pool.imap_unordered(
                    check_availability,
                    netstats.skip_dead_ips(retry_candidates(retry_queue, netstats))):
                process_reply(attempt, domain, ip, state, netstats, retry_queue, domain2ipset, ipsetcache)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
    multiprocessing.set_start_method('forkserver')
    domain2nsset, nsname2ipset, netstats, domain2ipset = load()
    try:
        update_mapping(domain2nsset, nsname2ipset, netstats, domain2ipset)
    except KeyboardInterrupt:
        pass
    finally:
        save(domain2nsset, netstats, domain2ipset)
    # machine readable output: # of domains which an working NS
    print(len(domain2ipset))
    # machine readable output: total # of domains with NS set
    print(len(domain2nsset))
