#!/usr/bin/python3
"""
Tranform DNS zone file into pickled Python objects.
"""

import argparse
import collections
import ipaddress
import logging
import pickle
import sys
from typing import Dict, FrozenSet, List, Iterable, Set, TextIO, Tuple

import dns.zone

import dataapi
from evalzone import AnIPAddress

class NameCache(collections.defaultdict):
    '''
    Dict: str -> dns.name.Name

    dns.name.from_text() is slow and this is used to cache its results
    '''
    def __missing__(self, name_str):
        name = dns.name.from_text(name_str)
        self[name_str] = name
        return name

class ZoneExtractor():
    def __init__(self, owner_str: str):
        # zone
        self.origin = dns.name.from_text(owner_str)
        self.nsnames = set()  # type: Set[dns.name.Name]
        self.nsname2ipset = {}  # type: Dict[dns.name.Name, FrozenSet[AnIPAddress]]
        self.domain2nsset = {}  # type: Dict[dns.name.Name, FrozenSet[dns.name.Name]]

        # data deduplication: temporary dict for singletons
        self.nssets = {}  # type: Dict[FrozenSet[dns.name.Name], FrozenSet[dns.name.Name]]
        self.names = NameCache()  # type: Dict[str, dns.name.Name]

        # reader
        self.buf_owner_str = owner_str
        self.buf_owner = dns.name.from_text(owner_str)
        self.buf_rdtype_str = 'SOA'
        self.buf_rdtexts = []  # type: List[str]

    def finish_rrset(self):
        if self.buf_rdtype_str == 'NS':
            nsset = frozenset(self.names[nsname] for nsname in self.buf_rdtexts)
            # use singleton frozenset to save memory, i.e. keep only single copy of frozensents with the same hash
            nsset = self.nssets.setdefault(nsset, nsset)
            self.domain2nsset[self.buf_owner] = nsset
            self.nsnames.update(nsset)
        elif self.buf_rdtype_str == 'ipaddr':
            ipset = frozenset(ipaddress.ip_address(ipaddr) for ipaddr in self.buf_rdtexts)
            self.nsname2ipset[self.buf_owner] = ipset
        elif self.buf_rdtype_str == 'SOA':
            pass  # ignore
        else:
            raise NotImplementedError('rdtype %s not supported' % self.buf_rdtype_str)
        self.buf_rdtexts = []

    def consume_line(self, line_owner_str, line_rdtype, line_rdtext):
        if line_rdtype == 'A' or line_rdtype == 'AAAA':
            line_rdtype = 'ipaddr'  # do not treat A/AAAA differently
        #print(self.buf_owner_str, self.buf_rdtype_str, self.buf_rdtexts)
        #print(line_owner_str, line_rdtype, line_rdtext)
        #set_trace()
        if self.buf_owner_str != line_owner_str:
            self.finish_rrset()
            self.buf_owner_str = line_owner_str
            # do not cache parsed owner name in dict, it is thrown away
            # after few zone file lines
            self.buf_owner = dns.name.from_text(line_owner_str)
        elif self.buf_rdtype_str != line_rdtype:
            # do not call finish twice!
            self.finish_rrset()
        self.buf_rdtype_str = line_rdtype

        # now buffer compatible data types
        self.buf_rdtexts.append(line_rdtext)

def _read_soa(zone_file: TextIO) -> str:
    line = zone_file.readline()
    owner, ttl, rdclass, rdtype, rdtext = line.split('\t', 5)
    assert(rdclass == 'IN')
    assert(rdtype == 'SOA')
    return owner

def convert(zone_file: TextIO, zone_origin: dns.name.Name) -> Tuple[ \
            Dict[dns.name.Name, FrozenSet[dns.name.Name]], \
            Set[dns.name.Name], \
            Dict[dns.name.Name, FrozenSet[AnIPAddress]]]:
    '''
    convert text zone into set of pickle files with preprocessed metadata

    only zone file canonicalized using this command is acceptable:
    ldns-read-zone -zc -E SOA -E NS -E A -E AAAA
    '''
    logging.info('loading and processing zone file')
    #input_size = os.fstat(zone_file.fileno()).st_size

    # read SOA
    origin_str = _read_soa(zone_file)
    zoneext = ZoneExtractor(origin_str)
    assert(zone_origin == zoneext.origin)

    for line in zone_file:
        owner_str, ttl, rdclass, rdtype, rdata = line.split('\t', 5)
        zoneext.consume_line(owner_str, rdtype, rdata.strip())
    zoneext.finish_rrset()

    return zoneext.domain2nsset, zoneext.nsnames, zoneext.nsname2ipset

def save(domain_nsset, nslist, nsname2ipset):
    '''
    generate pickle files with intermediate results
    '''
    logging.info('pickling NS sets')
    with open('domain2nsset.pickle', 'wb') as names2ns_pickle:
        pickle.dump(
            domain_nsset,
            names2ns_pickle
            )

    logging.info('pickling list of unique NS names')
    with open('nslist.pickle', 'wb') as nslist_pickle:
        pickle.dump(
            nslist,
            nslist_pickle
            )

    dataapi.save_nsname2ipset(nsname2ipset)

def main():
    '''stand-alone operation if allinone module is not used'''
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(message)s')
    argparser = argparse.ArgumentParser(description='parse text zone file into binary format')
    argparser.add_argument('zone_fn', type=open, help='zone file in RFC 1035 format')
    argparser.add_argument('zone_origin', type=dns.name.from_text, help='zone name, SOA RR must be present')
    args = argparser.parse_args()

    data = convert(args.zone_fn, args.zone_origin)
    save(*data)

if __name__ == "__main__":
    main()
