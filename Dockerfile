# see ci/Dockerfile
FROM registry.labs.nic.cz/knot/edns-zone-scanner/ci

# build Cythonized version of dnspython for Python 3 to speed operation on big zones
RUN dnf remove --noautoremove python3-dns -y
RUN dnf install python3-Cython /usr/lib/rpm/redhat/redhat-hardened-cc1 python3-devel -y
RUN pip3 install --install-option="--cython-compile" git+https://github.com/rthalley/dnspython.git

# copy scanner files to /usr/local/bin so they are easy to execute
COPY . /usr/local/bin

WORKDIR /data
ENTRYPOINT ["bash"]
